extern "C" 
{
#include <linkedList.h>
}
#include <cppunit/extensions/HelperMacros.h>  // CPPUNIT_TEST_SUITE, CPPUNIT_TEST

class LinkedListTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(LinkedListTest);

  CPPUNIT_TEST(testAddGetDataOnce);
  CPPUNIT_TEST(testAddGetDataNtime);

  CPPUNIT_TEST_SUITE_END();

 private:
  LinkedList *linked_list;

 public:
  void setUp() {
    linked_list = initLinkedList();
  }

  void tearDown() {
    deleteLinkedList(linked_list);
  }

  void testAddGetDataOnce() {
    // check result of initLinkedList()
    CPPUNIT_ASSERT(linked_list->data_num == 0);
    CPPUNIT_ASSERT(linked_list->first_data == NULL);
    CPPUNIT_ASSERT(linked_list->last_data == NULL);

    // execute addData()
    int *first_data = new int;
    *first_data = 101;
    LinkedListData *linked_list_data = addData(linked_list, first_data);

    // check result of addData()
    CPPUNIT_ASSERT(linked_list_data != NULL);
    CPPUNIT_ASSERT(linked_list->data_num == 1);
    CPPUNIT_ASSERT(linked_list->first_data == linked_list_data);
    CPPUNIT_ASSERT(linked_list->last_data == linked_list_data);

    // execute getData()
    LinkedListData *data_0 = getData(linked_list, 0);

    // check result of getData()
    CPPUNIT_ASSERT(data_0 != NULL);
    CPPUNIT_ASSERT(*(int *)(data_0->data) == 101);
    CPPUNIT_ASSERT(data_0->next == NULL);
  }

  void testAddGetDataNtime() {
    // check result of initLinkedList()
    CPPUNIT_ASSERT(linked_list->data_num == 0);
    CPPUNIT_ASSERT(linked_list->first_data == NULL);
    CPPUNIT_ASSERT(linked_list->last_data == NULL);

    // execute addData()
    int add_op_num = 5000;
    for (int dc = 0; dc < add_op_num; dc ++) {
      int *n_data = new int;
      *n_data = 100 + dc;
      LinkedListData *linked_list_data = addData(linked_list, n_data);

      // check result of addData()
      CPPUNIT_ASSERT(linked_list_data != NULL);
      CPPUNIT_ASSERT(linked_list->data_num == dc + 1);
    }

    // execute getData()
    for (int dc = 0; dc < linked_list->data_num; dc ++) {
      LinkedListData *data_n = getData(linked_list, dc);

      // check result of getData()
      CPPUNIT_ASSERT(*(int *)(data_n->data) == 100 + dc);
    }
  }
};
