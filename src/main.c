/*
 * Copyright 2018 Takuya Kitano
 */
#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"

int main() {
  LinkedList *linked_list = initLinkedList();

  // add data
  char *first_data = "This is first data";
  if (addData(linked_list, first_data)) {
    printf("add data successfully.\n");
    printf("first data : \"%s\"\n", linked_list->first_data->data);
    printf("last data : \"%s\"\n", linked_list->last_data->data);
  }

  // add data
  char *second_data = "This is second data";
  if (addData(linked_list, second_data)) {
    printf("add data successfully.\n");
    printf("first data : \"%s\"\n", linked_list->first_data->data);
    printf("next data : \"%s\"\n", linked_list->first_data->next->data);
    printf("last data : \"%s\"\n", linked_list->last_data->data);
  }

  // add data
  char *third_data = "This is third data";
  if (addData(linked_list, third_data)) {
    printf("add data successfully.\n");
    printf("first data : \"%s\"\n", linked_list->first_data->data);
    printf("second data : \"%s\"\n", linked_list->first_data->next->data);
    printf("third data : \"%s\"\n", linked_list->first_data->next->next->data);
    printf("last data : \"%s\"\n", linked_list->last_data->data);
  }

  LinkedListData *linked_list_data = getData(linked_list, 1);
  printf("## second data : %s\n", linked_list_data->data);

  int c = 0;
  for (c = 0; c < 1000; c ++) {
    if (addData(linked_list, second_data) == NULL) {
      printf("ERROR. Can't addData.");
      break;
    }
  }
  printf("data num : %d\n", linked_list->data_num);

  LinkedListData *linked_list_dataN = getData(linked_list, 500);
  printf("linked_list_dataN->data : %s\n", linked_list_dataN->data);
  deleteLinkedList(linked_list);
  return 0;
}
