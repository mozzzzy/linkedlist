/*
 * Copyright 2018 Takuya Kitano
 */
#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"

/*
 * createLinkedListData
 *
 * [Description]
 * Getting void *data, this function return pointer of 
 * LinkedListData structure.
 *
 * NOTE: We should execute free(linked_list_data)
 *       when we finnish using the LinkedListData that is created
 *       by this function.
 *
 * [Parameter]
 * - void *data
 *
 * [Return Value]
 * - LinkedListData *linked_list_data
 */
LinkedListData *createLinkedListData(void *data) {
  // allocate memory to new LinkedListData
  LinkedListData *linked_list_data =
    (LinkedListData *) malloc(sizeof(LinkedListData));

  // if allocate failed, return NULL
  if (linked_list_data == NULL) {
    return NULL;
  }

  // set data
  linked_list_data->data = data;
  linked_list_data->next = NULL;

  return linked_list_data;
}


/*
 * initLinkedList
 *
 * [Description]
 * This function create a new LinkedList.
 *
 * NOTE: We should execute deleteLinkedList function
 *       when we finnish using the LinkedList that is created
 *       by this function.
 *
 * [Parameter]
 * - No parameter
 *
 * [Return Value]
 * - LinkedList *linked_list
 */
LinkedList *initLinkedList() {
  LinkedList *linked_list = (LinkedList *) malloc(sizeof(LinkedList));
  linked_list->data_num = 0;
  linked_list->first_data = NULL;
  linked_list->last_data = NULL;

  return linked_list;
}


/*
 * deleteLinkedList
 *
 * [Description]
 * Getting pointer of LinkedList structure,
 * this function execute free function to each LinkedListData,
 * and LinkedList structure.
 *
 * [Parameter]
 * - LinkedList *linked_list
 *
 * [Return Value]
 * - No return value
 */
void deleteLinkedList(LinkedList *linked_list) {
  LinkedListData *linked_list_data = linked_list->first_data;
  while (1) {
    if (linked_list_data == NULL) {
      break;
    }

    LinkedListData *next_data = linked_list_data->next;
    free(linked_list_data);
    if (next_data == NULL) {
      break;
    } else {
      linked_list_data = next_data;
    }
  }

  free(linked_list);
}


/*
 * addData
 *
 * [Description]
 * Getting pointer of LinkedList structure and pointer of data,
 * this function appends data to the last of LinkedList.
 * This function returns pointer of LinkedListData structure
 * that is new created.
 *
 * [Parameter]
 * - LinkedList *linked_list
 * - void *data
 *
 * [Return Value]
 * - LinkedListData *linked_list_data
 */
LinkedListData *addData(LinkedList *linked_list, void *data) {
  // if linked_list has no data
  if (linked_list->data_num == 0 || linked_list->first_data == NULL) {
    // allocate memory to new LinkedListData
    LinkedListData *linked_list_data = createLinkedListData(data);
    // if allocate failed, return NULL
    if (linked_list_data == NULL) {
      return NULL;
    }

    // add to the list
    linked_list->first_data = linked_list_data;
    linked_list->last_data = linked_list_data;
    linked_list->data_num++;

    return linked_list_data;
  } else if (linked_list->data_num == 0) {
    // linked_list->data_num should not be 0 despite the fact that
    // linked_list->first_data is not NULL.
    return NULL;
  } else if (linked_list->first_data == NULL) {
    // linked_list->first_data should not be NULL despite the fact that
    // linked_list->data_num is not 0
    return NULL;
  } else {
    // allocate memory to new LinkedListData
    LinkedListData *linked_list_data = createLinkedListData(data);
    // if allocate failed, return NULL
    if (linked_list_data == NULL) {
      return NULL;
    }

    // add to the list
    linked_list->last_data->next = linked_list_data;
    linked_list->last_data = linked_list_data;
    linked_list->data_num++;

    return linked_list_data;
  }
}


/*
 * getData
 *
 * [Description]
 * Getting pointer of LinkedList structure and unsigned int position,
 * this function get the required position's data from LinkedList.
 *
 * [Parameter]
 * - LinkedList *linked_list
 * - unsigned int position
 *
 * [Return Value]
 * - LinkedListData *linked_list_data
 */
LinkedListData *getData(LinkedList *linked_list, unsigned int position) {
  if (linked_list->data_num < position) {
    return NULL;
  } else {
    int data_count;
    LinkedListData *linked_list_data = linked_list->first_data;
    for (data_count = 0; data_count <= position; data_count ++) {
      if (data_count == position) {
        return linked_list_data;
      } else {
        linked_list_data = linked_list_data->next;
      }
    }
    return NULL;
  }
}
