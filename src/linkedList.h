/*
 * Copyright 2018 Takuya Kitano
 */
#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

typedef struct LinkedListData {
  void *data;
  struct LinkedListData *next;
} LinkedListData;


typedef struct LinkedList {
  unsigned int data_num;
  struct LinkedListData *first_data;
  struct LinkedListData *last_data;
} LinkedList;


LinkedListData *createLinkedListData(void *data);
LinkedList *initLinkedList();
void deleteLinkedList(LinkedList *linked_list);
LinkedListData *addData(LinkedList *linked_list, void *data);
LinkedListData *getData(LinkedList *linked_list, unsigned int position);

#endif  // LINKEDLIST_H_
