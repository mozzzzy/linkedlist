#####################
###   variables   ###
#####################

#
# path to src/ and tests/ directory
#

CURRENT_DIR = $(shell pwd)
SRC_DIR = $(CURRENT_DIR)/src
TEST_DIR = $(CURRENT_DIR)/tests


#################
###   rules   ###
#################

#
# build rule
#

build: build_src build_test

build_src:
	$(MAKE) build -f $(SRC_DIR)/Makefile

build_test:
	$(MAKE) build -f $(TEST_DIR)/Makefile


#
# test rule
#

test:
	$(MAKE) test -f $(TEST_DIR)/Makefile


#
# clean rule
#

clean: clean_src clean_test

clean_src:
	$(MAKE) clean -f $(SRC_DIR)/Makefile

clean_test:
	$(MAKE) clean -f $(TEST_DIR)/Makefile


#
# install rule
#

install:
	$(MAKE) install -f $(SRC_DIR)/Makefile


#
# uninstall rule
#

uninstall:
	$(MAKE) uninstall -f $(SRC_DIR)/Makefile
